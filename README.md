# Section 5 Android Marathon

Task "Properties"

1. [Properties](https://gitlab.com/chd-k/section-5-android-marathon/-/tree/main/Properties)
1. [Lazy property](https://gitlab.com/chd-k/section-5-android-marathon/-/tree/main/Lazy%20property)
1. [Delegates examples](https://gitlab.com/chd-k/section-5-android-marathon/-/tree/main/Delegates%20examples)
1. [Delegates how it works](https://gitlab.com/chd-k/section-5-android-marathon/-/tree/main/Delegates%20how%20it%20works)
